#!/bin/bash
# Junicon test script
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m'
NC='\033[0m'
rm result.csv
touch result.csv
for f in *.icn; 
do
echo -e "${CYAN}Testing $f ${NC}"
FILENAME="${f%.*}"
junicon $f < data/$FILENAME.dat > test1.txt
#unicon $f -x < data/$FILENAME.dat > test3.txt
tail -n+3 test3.txt > test2.txt
rm $FILENAME
diff test1.txt stand/$FILENAME.std > result.txt
if [ -s result.txt ] 
then 
	if [ -s test1.txt ]
        then
                echo -e "${YELLOW}Incorrect output or runtime error${NC}"
                echo "$f,bad output/runtime error" >> result.csv
        else
                echo -e "${YELLOW}Compilation error${NC}"
                echo "$f,fail" >> result.csv

        fi
else
	echo -e "${GREEN}Test passed${NC}"
	echo "$f,0" >> result.csv
fi
rm test1.txt test2.txt result.txt 
done
